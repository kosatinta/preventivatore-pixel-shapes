import React from "react"
import QuotList from "./QuotList";

class QuotListGroup extends React.Component {
  render() {
    return (
      <div>
        {this.props.quotation_groups.map(quotation => (
          <QuotList
            key={quotation.id}
            quotation={quotation}
            handleChangeProps={this.props.handleChangeProps}
            deletequotationProps={this.props.deletequotationProps}
          />
        ))}
      </div>
    )
  }
}

export default QuotListGroup