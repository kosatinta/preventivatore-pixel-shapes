
import React from "react"

import Typography from '@material-ui/core/Typography';

const Header = () => {
  return (
    <Typography variant="h4" gutterBottom>Preventivatore Pixel Shapes</Typography>
  )
}

export default Header