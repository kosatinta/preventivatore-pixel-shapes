import React from "react"

import { v4 as uuidv4 } from "uuid";

import Container from '@material-ui/core/Container';

import QuotListGroup from "./QuotListGroup";
import Header from "./Header"
import FormQuotListGroup from "./FormQuotListGroup"
import PdfDocument from "./PdfDocument";


class QuotContainer extends React.Component {

  state = {
    header: {
      company: {
        name: "Pixel Shapes",
        adress: "Via Stesicoro, 62",
        email: "infopixelshapesgmail.com",
        website: "pixelshapes.it",
        phones: [
          {
            first_name: "Andrea",
            second_name: "Campo",
            phone: "+39 333 2954741"
          },
          {
            first_name: "Andrea",
            second_name: "Criscione",
            phone: "+39 333 2954741"
          },
          {
            first_name: "Giovanni",
            second_name: "Mezzasalma",
            phone: "+39 333 2954741"
          },
          {
            first_name: "Simone",
            second_name: "Scarpello",
            phone: "+39 333 2954741"
          }
        ]
      },
      customer: {
        name: "Cronos Scar Spa",
        adress: "Via Achille Grandi, 169",
        zipcode: "97100",
        city: "Ragusa",
        prov: "Rg",
        state: "Italy"
      },
    },
    quotation_groups: [
        // {
        // id: uuidv4(),
        // title: "Setup development environment",
        // completed: true
        // }
        // {
        //   id: uuidv4(),
        //   title: "Costi e sviluppi produzioni video e regia",
        //   items: [
        //     {
        //       id: uuidv4(),
        //       title: "rilievo e sopralluogo"
        //     },
        //     {
        //       id: uuidv4(),
        //       title: "realizzazione dei contenuti video"
        //     },
        //     {
        //       id: uuidv4(),
        //       title: "realizzazione dei contenuti audio"
        //     },
        //     {
        //       id: uuidv4(),
        //       title: "warping, blending e calibratura proiettori"
        //     }
        //   ],
        //   price: "300.000",
        //   currency: "Euro",
        //   iva: "22%"
        // }
    ]
  };

  addQuotListGroup = (title,price,list,forfetMode) => {
    const newQuotationListGroup = {
      id: uuidv4(),
      title: title,
      price: price,
      currency: "Euro",
      iva: "22%",
      list: list,
      forfet: forfetMode
    }
    this.setState({
      quotation_groups: [...this.state.quotation_groups, newQuotationListGroup]
    })
  }

  // handleChange = (id) => {
  //   this.setState(prevState => ({
  //     quotation_groups: prevState.quotation_groups.map((quotation) => {
  //       if(quotation.id === id){
  //         quotation.completed = !quotation.completed;
  //       }
  //       return quotation;
  //     })
  //   }))
  // };

  delQuotationGroup = (id) => {
    console.log('deleting', id);
    this.setState({
      quotation_groups: [
        ...this.state.quotation_groups.filter(quotation => {
          return quotation.id !== id;
        })
      ]
    })
  }

  render() {
    return (
      <Container fixed>
        <Header />
        <FormQuotListGroup addquotationGroupProps={this.addQuotListGroup} />
        <QuotListGroup
          quotation_groups={this.state.quotation_groups}
          handleChangeProps={this.handleChange}
          deletequotationProps={this.delQuotationGroup}
        />
        {/* <PdfDocument /> */}
      </Container>
    )
  }
}
export default QuotContainer