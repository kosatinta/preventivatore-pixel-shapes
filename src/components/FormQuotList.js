import React, { Component } from "react";

import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Grid,
    IconButton,
    TextField,
    Typography,
    TableRow,
    TableCell
} from '@material-ui/core/';

import AddIcon from '@material-ui/icons/Add';

class FormQuotList extends Component {

    state = {
        openDialog: false,
        dialogTitle: "",
        dialogMsg: "",
        dialogCloseBtnMsg: "Chiudi",
        itemtitle: "",
        uprice: 0,
        qty: 1
    }

    handleDialogClose = () => {
        this.setState({
            openDialog: false
        });
    };

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onAdd = e => {
        e.preventDefault();
        console.log(this.props);

        if (this.state.itemtitle == "") {
            this.setState({
                dialogTitle: "Titolo non inserito",
                dialogMsg: "Bisogna impostare almeno un titolo per inserire una voce di preventivo",
                openDialog: true
            })
            return
        }

        this.props.addquotationProps(
            this.state.itemtitle,
            this.state.uprice,
            this.state.qty
        );
        this.setState({
            itemtitle: "",
            uprice: 0,
            qty: 1
        })
    }

    calcRowTotal = () => {
        let rowTotal = parseInt(this.state.uprice) * parseInt(this.state.qty);
        return !isNaN(rowTotal) ? rowTotal : 0
    }

    render() {
        return (
            // <Typography variant="caption" >Aggiungi singole voci di spesa</Typography>
            <React.Fragment>
                <TableRow>
                    <TableCell>
                        <TextField fullWidth label="Titolo" name="itemtitle" value={this.state.itemtitle} onChange={this.onChange} />
                    </TableCell>
                    <TableCell>
                        <TextField label="Prezzo unitario" name="uprice" value={this.state.uprice} onChange={this.onChange} disabled={this.props.forfetMode} />
                    </TableCell>
                    <TableCell>
                        <TextField label="Quantita" name="qty" value={this.state.qty} onChange={this.onChange} disabled={this.props.forfetMode} />
                    </TableCell>
                    <TableCell>
                        <TextField label="Totale" name="tot" value={this.calcRowTotal()} onChange={this.onChange} disabled={this.props.forfetMode} />
                    </TableCell>
                    <TableCell>
                        <IconButton aria-label="Aggiungi voce" onClick={this.onAdd} disabled={this.props.forfetMode}>
                            <AddIcon />
                        </IconButton>
                    </TableCell>
                </TableRow>
                <Dialog
                    open={this.state.openDialog}
                    onClose={this.handleDialogClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{this.state.dialogTitle}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">{this.state.dialogMsg}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        {/* <Button onClick={this.handleDialogClose} color="primary">
                        Disagree
                    </Button> */}
                        <Button onClick={this.handleDialogClose} color="primary" autoFocus>
                            {this.state.dialogCloseBtnMsg}
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        )
    }
}
export default FormQuotList