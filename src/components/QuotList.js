import React from "react";
import { IconButton, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

class QuotList extends React.Component {
    render() {
        return (
            <Paper elevation={0} variant="outlined" square>
                {/* <input
                    type="checkbox"
                    checked={this.props.quotation.completed}
                    onChange={() => this.props.handleChangeProps(this.props.quotation.id)}
                /> */}
                {console.log('Quotation', this.props.quotation)}
                    <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell colSpan={this.props.quotation.forfet ? 1 : 4}>{this.props.quotation.title}</TableCell>
                                <TableCell>
                                    <IconButton style={{ float: 'right' }} onClick={() => this.props.deletequotationProps(this.props.quotation.id) }>
                                        <DeleteIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell>Desc.</TableCell>
                                {!this.props.quotation.forfet ? <TableCell>Q.</TableCell> : null }
                                {!this.props.quotation.forfet ? <TableCell>Prezzo un.</TableCell> : null }
                                {!this.props.quotation.forfet ? <TableCell>Totale</TableCell> : null }
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.quotation.list.map((item, index) => (
                                <TableRow key={`quotList-${index}`}>
                                    <TableCell>{index + 1}</TableCell>
                                    <TableCell>{item.itemtitle}</TableCell>
                                    {!this.props.quotation.forfet ? <TableCell>{item.qty}</TableCell> : null }
                                    {!this.props.quotation.forfet ? <TableCell>{item.uprice}</TableCell> : null }
                                    {!this.props.quotation.forfet ? <TableCell>{parseInt(item.qty)*parseInt(item.uprice)}</TableCell> : null }
                                </TableRow>
                            ))}
                            <TableRow>
                                <TableCell colSpan={this.props.quotation.forfet ? 1 : 4} />
                                <TableCell>
                                    {this.props.quotation.price} {this.props.quotation.currency}
                                    <Typography variant="caption">{this.props.quotation.iva ? ` Iva al ${this.props.quotation.iva}` : null}</Typography>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                    </TableContainer>
            </Paper>
        )
    }
}

export default QuotList