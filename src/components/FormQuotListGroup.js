import React, { Component } from "react";

import { v4 as uuidv4 } from "uuid";

import { 
    Typography,
    TextField,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Button,
    IconButton,
    Paper,
    Card,
    CardContent,
    Input,
    Switch,
    FormControlLabel,
} from "@material-ui/core";

import FormQuotList from "./FormQuotList"

import EditIcon from "@material-ui/icons/EditOutlined";
import DoneIcon from "@material-ui/icons/DoneAllTwoTone";
import RevertIcon from "@material-ui/icons/NotInterestedOutlined";

const inputPrice = React.createRef()


class FormQuotListGroup extends Component {
    
    state = {
        forfetMode: false,
        title: "",
        price: 0,
        quotationList: []
    }

    handleforfetModeSwitch = (event) => {
        this.setState({
            ...this.state.forfetMode, [event.target.name]: event.target.checked,
        });
      };

    onChange = e => {
        // if (...)this.inputPrice = 
        this.setState({
          [e.target.name]: e.target.value
        });
    }

    onSubmit = e => {
        e.preventDefault();
        this.props.addquotationGroupProps(
            this.state.title,
            this.state.price,
            this.state.quotationList,
            this.state.forfetMode,
        );
        this.setState({
            title: "",
            price: "",
            quotationList: [],
            forfetMode: false,
        })
    }

    addQuotList = (itemtitle,uprice,qty) => {
        // console.log('addQuotList title', itemtitle);
        // console.log('addQuotList price', uprice);
        // console.log('addQuotList qty', qty);
        const currPrice = parseInt(this.state.price)
        console.log('currPrice', currPrice)
        const newQuotationList = {
          id: uuidv4(),
          itemtitle: itemtitle,
          uprice: uprice,
          qty: qty,
          currency: "Euro",
          iva: "22%" 
        }
        this.setState({
          quotationList: [...this.state.quotationList, newQuotationList],
          price: currPrice + (parseInt(uprice) * parseInt(qty))
        })

      }

    renderQuotListItem = (quotationList) => {
        return (
            quotationList.map(quotation => (
                <TableRow key={quotation.id}>
                    <TableCell>{quotation.itemtitle}</TableCell>
                    <TableCell>{quotation.uprice}</TableCell>
                    <TableCell>{quotation.qty}</TableCell>
                    <TableCell>{parseInt(quotation.uprice) * parseInt(quotation.qty)}</TableCell>
                    <TableCell>
                        <IconButton>
                            <EditIcon />
                        </IconButton>
                    </TableCell>
                </TableRow>
            ))
        )
    }

    render() {
        return (
            <Card elevation={0} variant="outlined" square>
                <CardContent>
                <Typography variant="caption">Crea un gruppo di voci di spesa</Typography>
                <form onSubmit={this.onSubmit}>
                    <TextField fullWidth label="Titolo" name="title" value={this.state.title} onChange={this.onChange} />
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Des.</TableCell>
                                    <TableCell>Prezzo u.</TableCell>
                                    <TableCell>Q.</TableCell>
                                    <TableCell>Tot.</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.renderQuotListItem(this.state.quotationList)}
                                <FormQuotList addquotationProps={this.addQuotList} forfetMode={this.state.forfetMode} />
                                <TableRow>
                                    <TableCell colSpan={3} />
                                    <TableCell>
                                        <TextField label="Tot. gruppo" name="price" value={this.state.price} onChange={this.onChange} disabled={!this.state.forfetMode} />
                                    </TableCell>
                                    <TableCell>
                                        <FormControlLabel
                                            control={
                                            <Switch
                                                checked={this.state.forfetMode}
                                                onChange={this.handleforfetModeSwitch}
                                                name="forfetMode"
                                                color="primary"
                                            />
                                            }
                                            label="Forfet"
                                        />
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Button type="submit" variant="contained" color="primary" style={{ float: "right" }}>Aggiungi</Button>
                </form>
                </CardContent>
            </Card>
        )
    }
}
export default FormQuotListGroup