import React from "react"
import ReactDOM from "react-dom"
import 'fontsource-roboto'

import QuotContainer from "./components/QuotContainer"

ReactDOM.render(<QuotContainer />, document.getElementById("root"))